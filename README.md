# Trivia 💖
#### 🚀 Un projet réalisé par POMMIER Thibaud et IACCARINO Pierre 🚀
****
##### Installation

**Cloner le projet Gitlab :** 
```bash
git clone https://gitlab.com/Totiii/trivia.git
```

**Installation des dépendances :** 
```bash
npm install
```

**Lancer tous les tests réalisés :** 
```bash
npm test
```

****

*Ce projet dispose d'une license OpenSource.*

## Creative Commons
#### CC0
[![License: CC0-1.0](https://licensebuttons.net/l/zero/1.0/80x15.png)](http://creativecommons.org/publicdomain/zero/1.0/)  
`[![License: CC0-1.0](https://licensebuttons.net/l/zero/1.0/80x15.png)](http://creativecommons.org/publicdomain/zero/1.0/)`