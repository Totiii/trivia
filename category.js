class Category {
    constructor(name, number) {
        this.name = name
        this.number = number
        this.questions = []
    }

    addQuestion(question){
        this.questions.push(question)
    }

    askQuestion() {
        console.log(this.questions.shift().text)
    }
}

module.exports = Category;
