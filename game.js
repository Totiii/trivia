const Question = require("./question")

class Game {
  constructor(categories, players) {
    this.categories = categories
    this.players = players
    this.currentPlayer = 0
    this.isAWinner = false
  }

  getCurrentPlayer() {
    return this.players[this.currentPlayer]
  }

  nextPlayer() {
    this.currentPlayer += 1
    if (this.players.length === this.currentPlayer){
      this.currentPlayer = 0
    }
    return this.getCurrentPlayer()
  }

  isPlayable() {
    return this.players.length >= 2;
  }

  getCurrentCategory(place) {
    for(let category of this.categories){
      if(place % 4 === category.number){
        return category
      }
    }
    // we take the last category as a default
    return this.categories[this.categories.length - 1]
  }

  initQuestions(numberOfQuestion){
    for(let i = 0; i < numberOfQuestion; i++){
      this.categories.forEach((category) => {
        category.addQuestion(new Question(category.name + " Question " + i))
      })
    }
  }

  roll(roll) {
    let actualPlayer = this.getCurrentPlayer()
    console.log(actualPlayer.pseudo + " is the current player");
    console.log(actualPlayer.pseudo + " rolled a " + roll);

    if(actualPlayer.inJail){
      if(roll % 2 !== 0){
        actualPlayer.setInJail(false)
        actualPlayer.moveOnBoard(roll);
        let currentCategory = this.getCurrentCategory(actualPlayer.boardPlace)
        console.log("The category is " + currentCategory.name);
        currentCategory.askQuestion();
      }else{
        actualPlayer.setInJail(true)
      }
    }else{
      actualPlayer.moveOnBoard(roll);
      let currentCategory = this.getCurrentCategory(actualPlayer.boardPlace)
      console.log("The category is " + currentCategory.name);
      currentCategory.askQuestion();
    }
  }

  wasCorrectlyAnswered() {
    let player = this.getCurrentPlayer()
    if (player.inJail){
      player.setInJail(false)
    }
    console.log("Answer was correct!!!!");
    player.scoreAddOne();
    console.log(player.pseudo + " now has " + player.score  + " Gold Coins.");
    return player.didPlayerWin();
  }

  wrongAnswer() {
    let player = this.getCurrentPlayer()
    console.log('Question was incorrectly answered');
    player.setInJail(true)
    return false;
  }

  play() {
    if (!this.isPlayable()){
      console.log('Bad game settings, you can\'t play alone you young lonely guy')
    }else{
      this.initQuestions(50)
      do{
        this.roll(Math.floor(Math.random()*6) + 1);
        if(Math.floor(Math.random() * 10) === 7){
          this.isAWinner = this.wrongAnswer();
        }else{
          this.isAWinner = this.wasCorrectlyAnswered();
        }
        this.nextPlayer()
      }while(!this.isAWinner);
    }
  }
}

module.exports = Game;
