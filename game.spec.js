const Game = require("./game")
const Player = require("./player")
const Category = require("./category")
const mockPlayer = require("./player");

it("Game is playable", function() {
  const players = [
    new Player('test1'),
    new Player('test2')
  ];

  let game = new Game([], players);

  expect(game.isPlayable()).toBe(true);
});

it("Game is not playable", function() {
  const players = [
    new Player('test1')
  ];
  game = new Game([], players);
  expect(game.isPlayable()).toBe(false);
});

it("Test game get current player", function() {
  let player1 = new Player('test1')
  let player2 = new Player('test2')
  let game = new Game([], [player1, player2]);

  expect(game.getCurrentPlayer()).toBe(player1);

  game.currentPlayer += 1

  expect(game.getCurrentPlayer()).toBe(player2);
});

it("Test next player function", function() {
  let player1 = new Player('test1')
  let player2 = new Player('test2')
  let game = new Game([], [player1, player2]);

  expect(game.getCurrentPlayer()).toBe(player1);

  game.nextPlayer()
  expect(game.getCurrentPlayer()).toBe(player2);

  game.nextPlayer()
  expect(game.getCurrentPlayer()).toBe(player1);
});

it("Test get current category function", function() {
  let popCateg = new Category('Pop', 0)
  let rockCateg = new Category('Rock', 1)
  let game = new Game([popCateg, rockCateg], []);

  expect(game.getCurrentCategory(4)).toBe(popCateg);
  expect(game.getCurrentCategory(5)).toBe(rockCateg);
  expect(game.getCurrentCategory(6)).toBe(rockCateg);
});

it("Test the init questions function", async function() {
  let popCateg = new Category('Pop', 0)
  let rockCateg = new Category('Rock', 1)
  let game = new Game([popCateg, rockCateg], []);

  for (let category of game.categories){
    expect(category.questions.length).toEqual(0)
  }

  await game.initQuestions(50)

  for (let category of game.categories){
    expect(category.questions.length).toEqual(50)
  }
});

it("Test the roll function with a player that is not in jail", function() {
  let categPop = new Category('Pop', 0)
  const categories = [categPop, new Category('Science', 1)];
  let player1 = new Player('test1')
  const players = [player1, new Player('test2')];
  let game = new Game(categories, players);
  game.initQuestions(1)

  console.log = jest.fn();
  const spyPlayerMoveOnBoard  = jest.spyOn(player1, "moveOnBoard");
  const spyCategoryAskQuestion  = jest.spyOn(categPop, "askQuestion");
  const spyGameGetCurrentPlayer  = jest.spyOn(game, "getCurrentPlayer");
  const spyGameGetCurrentCategory  = jest.spyOn(game, "getCurrentCategory");

  game.roll(4)

  expect(spyGameGetCurrentPlayer).toHaveBeenCalled()
  expect(console.log).toHaveBeenCalledWith('test1 is the current player');
  expect(console.log).toHaveBeenCalledWith('test1 rolled a 4');
  expect(spyPlayerMoveOnBoard).toHaveBeenCalled()
  expect(spyGameGetCurrentCategory).toHaveBeenCalled()
  expect(console.log).toHaveBeenCalledWith('The category is Pop');
  expect(spyCategoryAskQuestion).toHaveBeenCalled()
});

it("Test the roll function with a player that is in jail and roll a odd", function() {
  let categScience = new Category('Science', 1)
  const categories = [new Category('Pop', 0), categScience];
  let player1 = new Player('test1')
  player1.inJail = true
  const players = [player1, new Player('test2')];
  let game = new Game(categories, players);
  game.initQuestions(1)

  console.log = jest.fn();
  const spyGameGetCurrentPlayer  = jest.spyOn(game, "getCurrentPlayer");
  const spyPlayerSetInJail  = jest.spyOn(player1, "setInJail");
  const spyPlayerMoveOnBoard  = jest.spyOn(player1, "moveOnBoard");
  const spyGameGetCurrentCategory  = jest.spyOn(game, "getCurrentCategory");
  const spyCategoryAskQuestion  = jest.spyOn(categScience, "askQuestion");

  game.roll(5)
  expect(spyGameGetCurrentPlayer).toHaveBeenCalled()
  expect(console.log).toHaveBeenCalledWith('test1 is the current player');
  expect(console.log).toHaveBeenCalledWith('test1 rolled a 5');
  expect(spyPlayerSetInJail).toHaveBeenCalled()
  expect(spyPlayerMoveOnBoard).toHaveBeenCalled()
  expect(spyGameGetCurrentCategory).toHaveBeenCalled()
  expect(console.log).toHaveBeenCalledWith('The category is Science');
  expect(spyCategoryAskQuestion).toHaveBeenCalled()
});

it("Test the roll function with a player that didn't roll a odd", function() {
  const categories = [new Category('Pop', 0), new Category('Science', 1)];
  let player1 = new Player('test1')
  player1.inJail = true
  const players = [player1, new Player('test2')];
  let game = new Game(categories, players);
  game.initQuestions(1)

  console.log = jest.fn();
  const spyGameGetCurrentPlayer  = jest.spyOn(game, "getCurrentPlayer");
  const spyPlayerSetInJail  = jest.spyOn(player1, "setInJail");

  game.roll(2)
  expect(spyGameGetCurrentPlayer).toHaveBeenCalled()
  expect(console.log).toHaveBeenCalledWith('test1 is the current player');
  expect(console.log).toHaveBeenCalledWith('test1 rolled a 2');
  expect(spyPlayerSetInJail).toHaveBeenCalled()
  expect(console.log).toHaveBeenCalledWith('test1 is in jail');
});

it("Test the was correctly answered function with a player not in jail", function() {
  let player1 = new Player('test1')
  const players = [player1, new Player('test2')];
  let game = new Game([], players);

  console.log = jest.fn();
  const spyGameGetCurrentPlayer  = jest.spyOn(game, "getCurrentPlayer");
  const spyPlayerScoreAddOne  = jest.spyOn(player1, "scoreAddOne");
  const spyPlayerDidPlayerWin  = jest.spyOn(player1, "didPlayerWin");

  game.wasCorrectlyAnswered()
  expect(spyGameGetCurrentPlayer).toHaveBeenCalled()
  expect(console.log).toHaveBeenCalledWith('Answer was correct!!!!');
  expect(spyPlayerScoreAddOne).toHaveBeenCalled()
  expect(console.log).toHaveBeenCalledWith("test1 now has 1 Gold Coins.");
  expect(spyPlayerDidPlayerWin).toHaveBeenCalled()
});

it("Test the was correctly answered function with a player in jail", function() {
  let player1 = new Player('test1')
  player1.inJail = true
  const players = [player1, new Player('test2')];
  let game = new Game([], players);

  console.log = jest.fn();
  const spyGameGetCurrentPlayer  = jest.spyOn(game, "getCurrentPlayer");
  const spyPlayerScoreAddOne  = jest.spyOn(player1, "scoreAddOne");
  const spyPlayerDidPlayerWin  = jest.spyOn(player1, "didPlayerWin");
  const spyPlayerSetInJail  = jest.spyOn(player1, "setInJail");

  game.wasCorrectlyAnswered()
  expect(spyGameGetCurrentPlayer).toHaveBeenCalled()
  expect(spyPlayerSetInJail).toHaveBeenCalled()
  expect(console.log).toHaveBeenCalledWith('test1 is out of jail');
  expect(console.log).toHaveBeenCalledWith('Answer was correct!!!!');
  expect(spyPlayerScoreAddOne).toHaveBeenCalled()
  expect(console.log).toHaveBeenCalledWith("test1 now has 1 Gold Coins.");
  expect(spyPlayerDidPlayerWin).toHaveBeenCalled()
});

it("Test the wrong answered function", function() {
  let player1 = new Player('test1')
  const players = [player1, new Player('test2')];
  let game = new Game([], players);

  console.log = jest.fn();
  const spyGameGetCurrentPlayer  = jest.spyOn(game, "getCurrentPlayer");
  const spyPlayerSetInJail  = jest.spyOn(player1, "setInJail");

  res = game.wrongAnswer()
  expect(spyGameGetCurrentPlayer).toHaveBeenCalled()
  expect(console.log).toHaveBeenCalledWith('Question was incorrectly answered');
  expect(spyPlayerSetInJail).toHaveBeenCalled()
  expect(console.log).toHaveBeenCalledWith('test1 is in jail');
  expect(res).toEqual(false)
});

