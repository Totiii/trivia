const Player = require("./player")
const Game = require("./game")
const Category = require("./category")

exports = typeof window !== "undefined" && window !== null ? window : global;
const categories = [new Category('Pop', 0), new Category('Science', 1), new Category('Sports', 2), new Category('Rock', 3)];
const players = [new Player('test1'), new Player('test2'), new Player('test3'), new Player('test4')];
const game = new Game(categories, players);

game.play()
