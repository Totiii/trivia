class Player{
    constructor(pseudo){
        this.pseudo = pseudo;
        this.score = 0;
        this.inJail = false;
        this.boardPlace = 0;
    }

    scoreAddOne(){
        this.score += 1;
    }

    setInJail(goToJail){
        this.inJail = goToJail;
        if(goToJail){
            console.log(this.pseudo + " is in jail");
        } else{
            console.log(this.pseudo + " is out of jail");
        }
    }

    moveOnBoard(diceRoll){
        this.boardPlace += diceRoll;
        if(this.boardPlace > 11){
            this.boardPlace = this.boardPlace - 12;
        }
        console.log(this.pseudo + "'s new location is " + this.boardPlace);
    }

    didPlayerWin(){
        return this.score === 6;
    }
}

module.exports = Player;
