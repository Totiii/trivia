const Player = require("./player")

it("Test the if score has been updated function", function() {
    let player = new Player("Test");

    expect(player.score).toBe(0);

    player.scoreAddOne()
    expect(player.score).toBe(1);

    player.scoreAddOne()
    player.scoreAddOne()
    player.scoreAddOne()
    expect(player.score).toBe(4);
});

it("Test the player constructor", function() {
    let player = new Player("Test");

    expect(player.score).toBe(0);
    expect(player.pseudo).toBe("Test");
    expect(player.inJail).toBe(false);
    expect(player.boardPlace).toBe(0);
});

it("Test the set in jail function", function() {
    let player = new Player("Test");
    console.log = jest.fn();

    expect(player.inJail).toBe(false);

    player.setInJail(false)
    expect(console.log).toHaveBeenCalledWith('Test is out of jail');
    expect(player.inJail).toBe(false);

    player.setInJail(true)
    expect(console.log).toHaveBeenCalledWith('Test is in jail');
    expect(player.inJail).toBe(true);
});

it("Test the move on board function", function() {
    let player = new Player("Test");
    console.log = jest.fn();

    expect(player.boardPlace).toBe(0);

    player.moveOnBoard(4)
    expect(console.log).toHaveBeenCalledWith('Test\'s new location is 4');
    expect(player.boardPlace).toBe(4);

    player.moveOnBoard(7)
    expect(console.log).toHaveBeenCalledWith('Test\'s new location is 11');
    expect(player.boardPlace).toBe(11);

    player.moveOnBoard(3)
    expect(console.log).toHaveBeenCalledWith('Test\'s new location is 2');
    expect(player.boardPlace).toBe(2);
});

it("Test the did player win function", function() {
    let player = new Player("Test");

    expect(player.didPlayerWin()).toBe(false);

    player.score = 6
    expect(player.didPlayerWin()).toBe(true);
});
