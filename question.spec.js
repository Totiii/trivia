const Category = require("./category")
const Game = require("./game")

it("Question not repeated twice", function() {
  const category = new Category('Pop', 0);
  let game = new Game([category], []);
  console.log = jest.fn();
  
  game.initQuestions(10);

  category.askQuestion()
  expect(console.log).toHaveBeenCalledWith('Pop Question 0');

  category.askQuestion()
  expect(console.log).toHaveBeenCalledWith('Pop Question 1');
});